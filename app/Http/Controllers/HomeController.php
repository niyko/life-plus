<?php

namespace App\Http\Controllers;

use App\Models\User;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Image;

class HomeController extends Controller
{
    public function homePage(){
        return view('pages.home');
    }

    public function changeStatus($status){
        User::where('id', Auth::id())->update(['is_donatable' => $status]);
        return redirect()->back();
    }
    
    public function findDonarPage(){
        return view('pages.find-donar');
    }

    public function findDonarResult(Request $request){
        $validated = $request->validate([
            'blood-group' => 'required',
            'city' => 'required'
        ]);
        $blood_group = $request->input('blood-group');
        $city = $request->input('city');
        $donars = User::whereIn('blood_group', $blood_group)
            ->where('city', $city)
            //->whereNot('id', Auth::id())
            ->where('is_donatable', '1')
            ->get();

        return view('pages.search-results', compact('donars'));
    }

    public function profilePage(){
        return view('pages.profile');
    }

    public function updateProfile(Request $request){
        $validated = $request->validate([
            'name' => 'required|min:5',
            'mobile' => 'required|digits:10',
            'blood-group' => 'required',
            'dob' => 'required|date_format:Y-m-d|before:-18 years',
            'place' => 'required',
            'city' => 'required'
        ]);

        User::where('id', Auth::id())
            ->update([
                'name' => $request->input('name'),
                'mobile' => $request->input('mobile'),
                'blood_group' => $request->input('blood-group'),
                'dob' => $request->input('dob'),
                'place' => $request->input('place'),
                'city' => $request->input('city'),
            ]);

        if($request->file('profile-pic')){
            $old_file = User::where('id', Auth::id())->first()->profile_pic;
            $file = $request->file('profile-pic');
            $file_name = Auth::id().'_'.uniqid().'.jpg';
            $file_path = public_path().'/uploads/profile/'.$file_name;
            $profile_pic = Image::make($file);
            $profile_pic->orientate();
            $profile_pic->fit(300, 300);
            $profile_pic->save($file_path);

            User::where('id', Auth::id())->update(['profile_pic' => $file_name]);
            if($old_file) Storage::disk('public')->delete('profile/'.$old_file);
        }

        return redirect()->back()->withErrors(['status' => 'Profile updated']);
    }

    public function removeProfilePic(){
        $old_file = User::where('id', Auth::id())->first()->profile_pic;
        User::where('id', Auth::id())->update(['profile_pic' => '']);
        if($old_file) Storage::disk('public')->delete('profile/'.$old_file);
        return redirect()->route('profile.page')->withErrors(['status' => 'Profile photo removed']);
    }

    public function changePasswordPage(){
        return view('pages.change-password');
    }

    public function changePassword(Request $request){
        $validated = $request->validate([
            'old-password' => 'required',
            'new-password' => 'required|min:7',
        ]);
        if(!Hash::check($request->input('old-password'), Auth::user()->password)) {
            return redirect()->back()->withErrors(['old-password' => 'Old password is wrong']);
        }
        User::where('email', Auth::user()->email)
            ->update([
                'password' => Hash::make($request->input('new-password'))
            ]);
        return redirect()->route('profile.page')->withErrors(['status' => 'Password has changed']);;
    }
}
