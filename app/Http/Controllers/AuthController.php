<?php

namespace App\Http\Controllers;

use App\Mail\ForgotPassword;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function loginPage(){
        return view('auth.login');
    }

    public function registerPage(){
        return view('auth.register');
    }

    public function loginUser(Request $request){
        $validated = $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:7',
        ]);

        $credentials = $request->only("email", "password");
        if (Auth::attempt($credentials, true)) return redirect()->route('home.page');
        else return redirect()->back()->withErrors(['email' => 'Email or password doesn\'t match']);
    }

    public function registerUser(Request $request){
        $validated = $request->validate([
            'name' => 'required|min:5',
            'email' => 'required|email',
            'mobile' => 'required|digits:10',
            'blood-group' => 'required',
            'dob' => 'required|date_format:Y-m-d|before:-18 years',
            'place' => 'required',
            'city' => 'required',
            'password' => 'required|min:7',
        ]);

        User::insert([
           'name' => $request->input('name'),
           'mobile' => $request->input('mobile'),
           'email' => $request->input('email'),
           'blood_group' => $request->input('blood-group'),
           'dob' => $request->input('dob'),
           'place' => $request->input('place'),
           'city' => $request->input('city'),
           'password' => Hash::make($request->input('password'))
        ]);

        $credentials = $request->only("email", "password");
        if (Auth::attempt($credentials, true)) return redirect()->route('home.page');
        else return redirect()->route('login.page');
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('login.page');
    }

    public function forgotPasswordPage(){
        return view('auth.forgot-password');
    }

    public function forgotPassword(Request $request){
        $validated = $request->validate([
            'email' => 'required|email'
        ]);
        $new_password = Str::random(10);
        $user = User::where('email', $request->input('email'))->first();
        if ($user === null) {
            return redirect()->back()->withErrors(['email' => 'Email doesn\'t match any account']);
        }
        User::where('email', $request->input('email'))
            ->update([
                'password' => Hash::make($new_password)
            ]);
        Mail::to($request->input('email'))->send(new ForgotPassword($new_password));
        return redirect()->route('login.page')->withErrors(['status' => 'Password sent to your email']);
    }
}
