@extends('app')

@push('head')
@endpush

@section('body')
{{-- <div class="nav-bar">
    <div uk-grid class="uk-grid-small">
        <div class="uk-width-auto uk-flex uk-flex-middle">
            <button class="nav-bar-back-btn ripples"><i class="icon">arrow_back</i></button>
        </div>
        <div class="uk-width-expand">
            <p class="nav-bar-back-title">Find Donar</p>
            <p class="nav-bar-back-sub-title">Someting about the page</p>
        </div>
    </div>
</div> --}}

<div class="auth-container" style="background-image: url('{{asset('images/bg-1.svg')}}');">
    <img class="auth-logo" src="{{asset('images/logo.svg')}}">
    <p class="auth-solgan">Be someone's second chance today</p>
    @yield('auth-form')
    <a class="auth-terms" href="https://niyko.com/privacy-policy/life-plus">By continuing you will accept out T&C</a>
</div>
@endsection

@push('script')
@endpush