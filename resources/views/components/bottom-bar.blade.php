<div class="bottom-bar uk-width-1-1 uk-box-shadow-medium">
    <div uk-grid class="uk-child-width-1-5 uk-grid-collapse">
        <div href="{{route('home.page')}}" class="uk-text-center bottom-bar-cell ripples">
            <button class="bottom-bar-icon {{request()->route()->getName()=='home.page'?'active':''}}"><i class="icon">{{(Auth::user()->is_donatable)?'opacity':'invert_colors_off'}}</i></button>
            <p class="bottom-bar-title {{request()->route()->getName()=='home.page'?'active':''}}">Home</p>
        </div>
        <div href="{{route('find.donar.page')}}" class="uk-text-center bottom-bar-cell ripples">
            <button class="bottom-bar-icon {{request()->route()->getName()=='find.donar.page'?'active':''}}"><i class="icon">person_search</i></button>
            <p class="bottom-bar-title {{request()->route()->getName()=='find.donar.page'?'active':''}}">Find</p>
        </div>
        <div class="uk-text-center bottom-bar-cell">
            <button href="{{route('find.donar.page')}}" class="bottom-bar-search-btn uk-box-shadow-medium ripples"><i class="icon">search</i></button>
        </div>
        <div href="{{route('profile.page')}}" class="uk-text-center bottom-bar-cell ripples">
            <button class="bottom-bar-icon {{request()->route()->getName()=='profile.page'?'active':''}}"><i class="icon">account_circle</i></button>
            <p class="bottom-bar-title {{request()->route()->getName()=='profile.page'?'active':''}}">Profile</p>
        </div>
        <div onclick="UIkit.modal('#logout-model').show();" onclcvsdvick="console.log('--app-exit')" class="uk-text-center bottom-bar-cell ripples">
            <button class="bottom-bar-icon"><i class="icon">logout</i></button>
            <p class="bottom-bar-title">Logout</p>
        </div>
    </div>
</div>

<div id="logout-model" class="confirmation-modal" uk-modal>
    <div class="uk-modal-dialog uk-modal-body confirmation-modal-padding">
        <h3>Are you sure that you<br>want to signout</h3>
        <p class="uk-text-right">
            <button onclick="UIkit.modal('#logout-model').hide();" class="btn btn-accent btn-small" type="button">Cancel</button>
            <button href="{{route('logout')}}" class="btn btn-primary btn-small" type="button">Signout</button>
        </p>
    </div>
</div>