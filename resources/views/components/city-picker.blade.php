<input name="city" class="uk-width-1-1" readonly type="text" uk-toggle="target: #city-picker-modal" placeholder="eg. Ernakulam" required autocomplete="off" value="{{old('city')?old('city'):$value}}">
@error('city')
    <label class="input-error">{{ $message }}</label>
@enderror

<div id="city-picker-modal" uk-modal class="city-picker-container">
    <div class="nav-bar nav-bar-fixed uk-box-shadow-medium">
        <div uk-grid class="uk-grid-small">
            <div class="uk-width-auto uk-flex uk-flex-middle">
                <button class="nav-bar-back-btn ripples"><i class="icon">arrow_back</i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav-bar-back-title">Pick City</p>
                <p class="nav-bar-back-sub-title">Select the city you want</p>
            </div>
        </div>
    </div>

    <div class="uk-modal-dialog uk-modal-body" id="cities">
        <div class="nav-bar-padding-top uk-margin-small-top">
            <div class="input-container list-padding uk-margin-small-top">
                <input class="search uk-width-1-1 uk-margin-small-top" type="text" placeholder="Search" autocomplete="off">
            </div>

            <ul class="list">
                @foreach(App\Models\City::all() as $city)
                    <li onclick="pickCity('{{$city->name}}')" class="list-padding ripples"><span class="name">{{$city->name}}</span></li>
                @endforeach
            </li>
        </div>
    </div>
</div>

@push('script')
<script>
    let citiesList = new List("cities", {
        valueNames: ["name"]
    });

    const pickCity = (cityName) =>{
        $("[name=city]").val(cityName);
        UIkit.modal("#city-picker-modal").hide();
    }
</script>
@endpush