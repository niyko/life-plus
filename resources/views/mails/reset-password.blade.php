<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@100;200;300;400;500;600;700&display=swap" rel="stylesheet">
        <style>
            u+#body a {
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }

            .ii a[href] {
                color: inherit !important;
            }

            .mail-container{
                margin-bottom: 50px
            }

            .mail-logo{
                width: 120px;
                margin: 0px auto;
                display: block;
                margin-top: 60px;
            }

            .mail-title{
                color: #303B4B;
                font-family: 'Heebo', sans-serif;
                text-align: center;
                font-weight: 500;
                font-size: 1.4rem;
                margin: 0;
                margin-top: 35px;
            }

            .mail-desc{
                font-family: 'Heebo', sans-serif;
                margin: 0;
                text-align: center;
                font-weight: 500;
                color: #303B4B;
                opacity: 0.8;
                margin-bottom: 40px;
            }

            .mail-content{
                width: 330px;
                font-size: 14px;
                margin: 0px auto 35px auto;
                text-align: justify;
                color: #303B4B7D;
                font-family: 'Heebo', sans-serif;
            }

            .mail-btn{
                display: block;
                margin: 0px auto;
                width: max-content;
                background: #de2c2c1c;
                color: #de2c2c !important;
                text-decoration: none;
                padding: 10px 15px;
                font-weight: 600;
                font-size: 16px;
                border-radius: 5px;
                font-family: 'Heebo', sans-serif;
                border: 3px dashed #de2c2c8c;
            }

            .mail-text-center{
                text-align: center;
            }

            .mail-link{
                font-size: 11px;
                color: #303B4B7D !important;
                font-family: 'Heebo', sans-serif;
                text-align: center;
                display: block;
                font-weight: 500;
                margin-top: 27px;
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        <div class="mail-container">
            <img src="https://i.imgur.com/znTBj7R.png" class="mail-logo"/>
            <h3 class="mail-title">Reset Password</h3>
            <p class="mail-desc">Your password has been changed</p>
            <p class="mail-content">New assigned password is given below in the box, Use this password to sign in to your account. You can change this password afterwards from profile.</p>
            <p class="mail-btn" href="">{{$password}}</p>
        </div>
    </body>
</html>