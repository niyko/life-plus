@extends('layouts.auth')

@push('head')
    <link href="{{asset('css/auth.css')}}?integrity={{integrity('css/auth.css')}}" rel="stylesheet">
@endpush

@section('auth-form')
<div>
    <form action="{{route('register.user')}}" method="POST">
        @csrf
        <h2 class="auth-title">Get Started Now!</h2>
        <div class="input-container">
            <label>Your Name</label>
            <input name="name" class="uk-width-1-1" type="text" placeholder="eg. Candace Willams" required autocomplete="off" value="{{old('name')}}">
            @error('name')
                <label class="input-error">{{ $message }}</label>
            @enderror
        </div>

        <div class="input-container uk-margin-top">
            <label>Your Email</label>
            <input name="email" class="uk-width-1-1" type="email" placeholder="eg. hermione@gryffindor.com" required autocomplete="off" value="{{old('email')}}">
            @error('email')
                <label class="input-error">{{ $message }}</label>
            @enderror
        </div>

        <div class="input-container uk-margin-top">
            <label>Your Mobile</label>
            <input name="mobile" class="uk-width-1-1" type="text" placeholder="(+91)" pattern="[7-9]{1}[0-9]{9}" title="Valid mobile number without (+91)" required autocomplete="off" value="{{old('mobile')}}">
            @error('mobile')
                <label class="input-error">{{ $message }}</label>
            @enderror
        </div>

        <div class="input-container uk-margin-top">
            <label>Your Blood Group</label>
            <select name="blood-group" class="uk-width-1-1" required autocomplete="off">
                <option disabled selected>Pick one</option>
                <option {{old('blood-group')=='A+'?'selected':''}} value="A+">A RhD positive (A+)</option>
                <option {{old('blood-group')=='A-'?'selected':''}} value="A-">A RhD negative (A-)</option>
                <option {{old('blood-group')=='B+'?'selected':''}} value="B+">B RhD positive (B+)</option>
                <option {{old('blood-group')=='B-'?'selected':''}} value="B-">B RhD negative (B-)</option>
                <option {{old('blood-group')=='O+'?'selected':''}} value="O+">O RhD positive (O+)</option>
                <option {{old('blood-group')=='O-'?'selected':''}} value="O-">O RhD negative (O-)</option>
                <option {{old('blood-group')=='AB+'?'selected':''}} value="AB+">AB RhD positive (AB+)</option>
                <option {{old('blood-group')=='AB-'?'selected':''}} value="AB-">AB RhD negative (AB-)</option>
            </select>
            @error('blood-group')
                <label class="input-error">{{ $message }}</label>
            @enderror
        </div>

        <div class="input-container uk-margin-top">
            <label>Date of Birth</label>
            <input name="dob" class="uk-width-1-1" type="date" placeholder="DD/MM/YYYY" required autocomplete="off" value="{{old('dob')}}">
            @error('dob')
                <label class="input-error">{{ $message }}</label>
            @enderror
        </div>

        <div class="input-container uk-margin-top">
            <label>Town / City</label>
            @include('components.city-picker', ['value'=>''])
        </div>

        <div class="input-container uk-margin-top">
            <label>You live in</label>
            <input name="place" class="uk-width-1-1" type="text" placeholder="eg. Kochi" required autocomplete="off" value="{{old('place')}}">
            @error('place')
                <label class="input-error">{{ $message }}</label>
            @enderror
        </div>

        <div class="input-container uk-margin-top">
            <label>New Password</label>
            <input name="password" class="uk-width-1-1" type="password" required autocomplete="off">
            @error('password')
                <label class="input-error">{{ $message }}</label>
            @enderror
        </div>

        <button class="btn btn-primary uk-width-1-1 uk-margin-top ripples" type="submit">Create Account</button>
        <button href="{{route('login.page')}}" class="btn btn-accent uk-width-1-1 uk-margin-small-top ripples" type="button">I'm a existing user, Sign in</button>
    </form>
</div>
@endsection

@push('script')
@endpush