@extends('layouts.auth')

@push('head')
    <link href="{{asset('css/auth.css')}}?integrity={{integrity('css/auth.css')}}" rel="stylesheet">
@endpush

@section('auth-form')
<div>
    <form action="{{route('forgot.password')}}" method="POST">
        @csrf
        <h2 class="auth-title">Reset Password</h2>
        <div class="input-container">
            <label>Your Email</label>
            <input name="email" class="uk-width-1-1" type="email" required autocomplete="off" value="{{old('email')}}">
            @error('email')
                <label class="input-error">{{ $message }}</label>
            @enderror
        </div>

        <button class="btn btn-primary uk-width-1-1 uk-margin-top ripples" type="submit">Reset Password</button>
        <button href="{{route('login.page')}}" class="btn btn-accent uk-width-1-1 uk-margin-small-top ripples" type="button">I remember it, Sign in</button>
    </form>
</div>
@endsection

@push('script')
@endpush