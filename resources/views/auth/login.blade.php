@extends('layouts.auth')

@push('head')
    <link href="{{asset('css/auth.css')}}?integrity={{integrity('css/auth.css')}}" rel="stylesheet">
@endpush

@section('auth-form')
<div>
    <form action="{{route('login.user')}}" method="POST">
        @csrf
        <h2 class="auth-title">Welcome Back!</h2>
        <div class="input-container">
            <label>Your Email</label>
            <input name="email" class="uk-width-1-1" type="email" required autocomplete="off" value="{{old('email')}}">
            @error('email')
                <label class="input-error">{{ $message }}</label>
            @enderror
        </div>

        <div class="input-container uk-margin-top">
            <label>Password</label>
            <input name="password" class="uk-width-1-1" type="password" required autocomplete="off" value="{{old('password')}}">
            @error('password')
                <label class="input-error">{{ $message }}</label>
            @enderror
        </div>

        <a href="{{route('forgot.password.page')}}" class="auth-forgot-link uk-margin-top" href="#">Forgot Password</a>
        <button class="btn btn-primary uk-width-1-1 uk-margin-top ripples" type="submit">Sign in</button>
        <button href="{{route('register.page')}}" class="btn btn-accent uk-width-1-1 uk-margin-small-top ripples" type="button">I'm a new user, Sign up</button>
    </form>
</div>

@error('status')
    @push('script')
        <script>
            UIkit.notification({
                message: '{{$message}}',
                pos: 'bottom-center'
            });
        </script>
    @endpush
@enderror

@endsection

@push('script')
@endpush