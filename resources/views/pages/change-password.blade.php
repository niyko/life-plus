@extends('app')

@push('head')
    <link href="{{asset('css/profile.css')}}?integrity={{integrity('css/profile.css')}}" rel="stylesheet">
@endpush

@section('body')
<div class="nav-bar nav-bar-fixed uk-box-shadow-medium">
    <div uk-grid class="uk-grid-small">
        <div class="uk-width-auto uk-flex uk-flex-middle">
            <button href="{{route('profile.page')}}" class="nav-bar-back-btn ripples"><i class="icon">arrow_back</i></button>
        </div>
        <div class="uk-width-expand">
            <p class="nav-bar-back-title">Change Password</p>
            <p class="nav-bar-back-sub-title">Update your account password</p>
        </div>
    </div>
</div>

<div class="app-container">
    <div class="nav-bar-padding-top">
        <form action="{{route('password.profile.change')}}" method="POST" enctype="multipart/form-data">
            <div class="input-container uk-margin-top">
                <label>Your old password</label>
                <input name="old-password" class="uk-width-1-1" type="password" required autocomplete="off">
                @error('old-password')
                    <label class="input-error">{{ $message }}</label>
                @enderror
            </div>

            <div class="input-container uk-margin-top">
                <label>Your new password</label>
                <input name="new-password" class="uk-width-1-1" type="password" required autocomplete="off">
                @error('new-password')
                    <label class="input-error">{{ $message }}</label>
                @enderror
            </div>

            <button class="btn btn-primary uk-width-1-1 uk-margin-top ripples" type="submit">Update</button>
        </form>
    </div>
</div>

@include('components.bottom-bar')
@endsection

@push('script')
@endpush