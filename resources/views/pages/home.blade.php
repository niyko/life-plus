@extends('app')

@push('head')
    <link href="{{asset('css/home.css')}}?integrity={{integrity('css/home.css')}}" rel="stylesheet">
@endpush

@section('body')
<div class="nav-bar">
    <div uk-grid class="uk-grid-small">
        <div class="uk-width-expand">
            <img class="nav-bar-logo" src="{{asset('images/logo.svg')}}">
        </div>
        <div class="uk-width-auto uk-flex uk-flex-middle">
            <img href="{{route('profile.page')}}" class="nav-bar-user-icon ripples" src="{{(Auth::user()->profile_pic)?asset('uploads/profile/'.Auth::user()->profile_pic):'https://ui-avatars.com/api/?size=100&name='.Auth::user()->name}}">
        </div>
    </div>
</div>

<div class="app-container">
    <div class="home-container uk-flex uk-flex-middle" style="background-image: url('{{asset('images/home-toggle-bg.svg?434')}}')">
        <div class="uk-text-center uk-width-1-1">
            <button href="{{route('change.status', ['status'=>!Auth::user()->is_donatable?'1':'0'])}}" class="home-toggle-btn ripples {{(Auth::user()->is_donatable)?'active':''}}"><i class="icon">{{(Auth::user()->is_donatable)?'invert_colors':'invert_colors_off'}}</i></button>
        </div>
    </div>
    <div class="home-toggle-text-container uk-flex uk-flex-middle">
        <div class="uk-width-1-1">
            <p class="home-toggle-label">Your status</p>
            <p class="home-toggle-status">{{(Auth::user()->is_donatable)?'I\'m ready to donate':'Can\'t donate now'}}</p>
            <p class="home-toggle-desc">Click to change status</p>
        </div>
    </div>
</div>

@include('components.bottom-bar')
@endsection

@push('script')
@endpush