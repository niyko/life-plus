@extends('app')

@push('head')
    <link href="{{asset('css/find-donar.css')}}?integrity={{integrity('css/find-donar.css')}}" rel="stylesheet">
@endpush

@section('body')
<div class="nav-bar uk-box-shadow-medium">
    <div uk-grid class="uk-grid-small">
        <div class="uk-width-auto uk-flex uk-flex-middle">
            <button href="{{route('home.page')}}" class="nav-bar-back-btn ripples"><i class="icon">arrow_back</i></button>
        </div>
        <div class="uk-width-expand">
            <p class="nav-bar-back-title">Find Donar</p>
            <p class="nav-bar-back-sub-title">Search for blood donars around you</p>
        </div>
    </div>
</div>

<div class="app-container">
    <form action="{{route('find.donar.result')}}" method="POST">
        @csrf
        <div class="input-container uk-margin-top">
            <label>Choose Blood Group</label>
            <div class="find-donar-checkbox uk-box-shadow-small uk-margin-small-top ripples">
                <div uk-grid class="uk-grid-collapse uk-child-width-1-5">
                    @foreach(['A+', 'A-', 'B+', 'B-', 'O+'] as $blood_group)
                        <div>
                            <input name="blood-group[]" id="blood{{$blood_group}}" type="checkbox" value="{{$blood_group}}" autocomplete="off">
                            <label class="checkbox-label" for="blood{{$blood_group}}">{{$blood_group}}</label>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="find-donar-checkbox uk-box-shadow-small uk-margin-small-top ripples">
                <div uk-grid class="uk-grid-collapse uk-child-width-1-5">
                    @foreach(['O-', 'AB+', 'AB-'] as $blood_group)
                        <div>
                            <input name="blood-group[]" id="blood{{$blood_group}}" type="checkbox" value="{{$blood_group}}">
                            <label class="checkbox-label" for="blood{{$blood_group}}">{{$blood_group}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @error('blood-group')
            <label class="input-error uk-margin-small-top">{{ $message }}</label>
        @enderror

        <div class="input-container uk-margin-top">
            <label>Town / City</label>
            @include('components.city-picker', ['value'=>''])
        </div>

        <button class="btn btn-primary uk-width-1-1 uk-margin-top ripples" type="submit">Search</button>
    </form>
</div>

@include('components.bottom-bar')
@endsection

@push('script')
@endpush