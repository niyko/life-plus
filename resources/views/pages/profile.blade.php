@extends('app')

@push('head')
    <link href="{{asset('css/profile.css')}}?integrity={{integrity('css/profile.css')}}" rel="stylesheet">
@endpush

@section('body')
<div class="nav-bar nav-bar-fixed uk-box-shadow-medium">
    <div uk-grid class="uk-grid-small">
        <div class="uk-width-auto uk-flex uk-flex-middle">
            <button href="{{route('home.page')}}" class="nav-bar-back-btn ripples"><i class="icon">arrow_back</i></button>
        </div>
        <div class="uk-width-expand">
            <p class="nav-bar-back-title">Your Profile</p>
            <p class="nav-bar-back-sub-title">Change your profile details</p>
        </div>
    </div>
</div>

<div class="app-container">
    <div class="nav-bar-padding-top">
        <form oninput="showSubmitBtn()" action="{{route('update.profile')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="profile-pic-container uk-margin-top" onclick="UIkit.modal('#profile-pic-modal').show();">
                <img class="profile-pic" src="{{(Auth::user()->profile_pic)?asset('uploads/profile/'.Auth::user()->profile_pic):'https://ui-avatars.com/api/?size=100&name='.Auth::user()->name}}">
                <button class="ripples" type="button"><i class="icon">camera_alt</i></button>
            </div>

            <input name="profile-pic" onchange="readLocalImage(this)" type="file">

            <div class="input-container">
                <label>Your Name</label>
                <input name="name" class="uk-width-1-1" type="text" placeholder="{{Auth::user()->name}}" required autocomplete="off" value="{{old('name')?old('name'):Auth::user()->name}}">
                @error('name')
                    <label class="input-error">{{ $message }}</label>
                @enderror
            </div>

            <div class="input-container uk-margin-top">
                <label>Your Email</label>
                <input name="email" disabled class="uk-width-1-1" type="email" required autocomplete="off" value="{{Auth::user()->email}}">
            </div>

            <div class="input-container uk-margin-top">
                <label>Your Mobile</label>
                <input name="mobile" class="uk-width-1-1" type="text" placeholder="{{Auth::user()->mobile}}" pattern="[7-9]{1}[0-9]{9}" title="Valid mobile number without (+91)" required autocomplete="off" value="{{old('mobile')?old('mobile'):Auth::user()->mobile}}">
                @error('mobile')
                    <label class="input-error">{{ $message }}</label>
                @enderror
            </div>

            <div class="input-container uk-margin-top">
                <label>Your Blood Group</label>
                <select name="blood-group" class="uk-width-1-1" required autocomplete="off">
                    <option disabled selected>Pick one</option>
                    <option {{old('blood-group')?old('blood-group'):Auth::user()->blood_group=='A+'?'selected':''}} value="A+">A RhD positive (A+)</option>
                    <option {{old('blood-group')?old('blood-group'):Auth::user()->blood_group=='A-'?'selected':''}} value="A-">A RhD negative (A-)</option>
                    <option {{old('blood-group')?old('blood-group'):Auth::user()->blood_group=='B+'?'selected':''}} value="B+">B RhD positive (B+)</option>
                    <option {{old('blood-group')?old('blood-group'):Auth::user()->blood_group=='B-'?'selected':''}} value="B-">B RhD negative (B-)</option>
                    <option {{old('blood-group')?old('blood-group'):Auth::user()->blood_group=='O+'?'selected':''}} value="O+">O RhD positive (O+)</option>
                    <option {{old('blood-group')?old('blood-group'):Auth::user()->blood_group=='O-'?'selected':''}} value="O-">O RhD negative (O-)</option>
                    <option {{old('blood-group')?old('blood-group'):Auth::user()->blood_group=='AB+'?'selected':''}} value="AB+">AB RhD positive (AB+)</option>
                    <option {{old('blood-group')?old('blood-group'):Auth::user()->blood_group=='AB-'?'selected':''}} value="AB-">AB RhD negative (AB-)</option>
                </select>
                @error('blood-group')
                    <label class="input-error">{{ $message }}</label>
                @enderror
            </div>

            <div class="input-container uk-margin-top">
                <label>Date of Birth</label>
                <input name="dob" class="uk-width-1-1" type="date" placeholder="{{Auth::user()->dob}}" required autocomplete="off" value="{{old('dob')?old('dob'):Auth::user()->dob}}">
                @error('dob')
                    <label class="input-error">{{ $message }}</label>
                @enderror
            </div>

            <div class="input-container uk-margin-top">
                <label>Town / City</label>
                @include('components.city-picker', ['value'=>Auth::user()->city])
            </div>

            <div class="input-container uk-margin-top">
                <label>You live in</label>
                <input name="place" class="uk-width-1-1" type="text" placeholder="{{Auth::user()->place}}" required autocomplete="off" value="{{old('place')?old('place'):Auth::user()->place}}">
                @error('place')
                    <label class="input-error">{{ $message }}</label>
                @enderror
            </div>

            <button class="btn btn-primary uk-width-1-1 uk-margin-top ripples hide" type="submit">Update</button>
            <button href="{{route('password.profile.page')}}" class="btn btn-accent uk-width-1-1 uk-margin-small-top ripples" type="button">Change Password</button>
        </form>
    </div>
</div>

<div id="profile-pic-modal" class="confirmation-modal" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <ul class="list">
            <li class="ripples" onclick="pickImage()"><i class="icon">camera_alt</i> Upload new photo</li>
            <li href="{{route('remove.profile.pic')}}" class="ripples"><i class="icon">remove_circle</i> Remove photo</li>
        </li>
    </div>
</div>

@error('status')
    @push('script')
        <script>
            UIkit.notification({
                message: '{{$message}}',
                pos: 'bottom-center'
            });
        </script>
    @endpush
@enderror

@include('components.bottom-bar')
@endsection

@push('script')
<script>
    const showSubmitBtn = () =>{
        $("button[type=submit]").fadeIn();
    }

    const readLocalImage = (input) =>{
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(".profile-pic").attr("src", e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    const pickImage = () =>{
        $('input[type=file]').click();
        UIkit.modal('#profile-pic-modal').hide();
    }
</script>
@endpush