@extends('app')

@push('head')
    <link href="{{asset('css/search-results.css')}}?integrity={{integrity('css/search-results.css')}}" rel="stylesheet">
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
@endpush

@section('body')
<div class="nav-bar nav-bar-fixed uk-box-shadow-medium">
    <div uk-grid class="uk-grid-small">
        <div class="uk-width-auto uk-flex uk-flex-middle">
            <button href="{{route('find.donar.page')}}" class="nav-bar-back-btn ripples"><i class="icon">arrow_back</i></button>
        </div>
        <div class="uk-width-expand">
            <p class="nav-bar-back-title">Donar List</p>
            <p class="nav-bar-back-sub-title">Search for blood donars around you</p>
        </div>
    </div>
</div>

<div class="app-container">
    <div class="nav-bar-padding-top">
        @foreach($donars as $donar)
            <div class="search-result-item uk-margin-top">
                <div uk-grid class="uk-grid-small">
                    <div class="uk-width-auto">
                        <button class="donar-list-chip">{{$donar->blood_group}}</button>
                    </div>
                    <div class="uk-width-expand">
                        <p class="donar-list-title">{{$donar->name}}</p>
                        <p class="donar-list-age">{{\Carbon\Carbon::parse($donar->dob)->diff(\Carbon\Carbon::now())->format('%y years old')}}</p>
                        <p class="donar-list-place">{{$donar->place}}, {{$donar->city}}</p>
                        <button href="tel:{{$donar->mobile}}" class="donar-list-btn ripples"><i class="icon">phone</i> Call Donar</button>
                    </div>
                    <div class="uk-width-auto">
                        <img class="donar-list-img" src="{{($donar->profile_pic)?asset('uploads/profile/'.$donar->profile_pic):'https://ui-avatars.com/api/?size=100&name='.$donar->name}}">
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    
    @if($donars->isEmpty())
        <div class="donar-empty-container uk-flex uk-flex-middle">
            <div class="uk-width-1-1">
                <lottie-player class="donar-empty-img" src="https://assets2.lottiefiles.com/temp/lf20_ybprWS.json" background="transparent" speed="1" loop autoplay></lottie-player>
                <p class="donar-empty-title">No donars found</p>
            </div>
        </div>
    @endif
</div>

@include('components.bottom-bar')
@endsection

@push('script')
@endpush