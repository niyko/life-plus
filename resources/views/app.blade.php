<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" id="csrf-token" content="{{csrf_token()}}">
        <title>Life Plus</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/css/uikit.min.css" />
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@100;200;300;400;500;600;700&display=swap" rel="stylesheet"> 
        <link href="{{asset('css/ripple.css')}}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{asset('css/main.css')}}?integrity={{integrity('css/main.css')}}" rel="stylesheet">
        @stack('head')
    </head>
    <body>
        @yield('body')
        
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.9/dist/js/uikit.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
        <script src="{{asset('js/ripple.js')}}"></script>
        <script src="{{asset('js/moment.min.js')}}"></script>
        <script src='https://cdn.jsdelivr.net/gh/Niyko/href-For-EveryThing/src/hrefForEveryThing.js'></script>
        <script src="{{asset('js/main.js')}}?integrity={{integrity('js/main.js')}}"></script>
        <script>
            $(function() {
                $.ripple(".ripples", {
                    debug: false, 
                    opacity: 0.2,
                    color: "auto",
                    multi: true
                });
            });
        </script>
        @stack('script')
    </body>
</html>
