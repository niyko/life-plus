<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'homePage'])->name('home.page')->middleware('auth');
Route::get('/find-donar', [HomeController::class, 'findDonarPage'])->name('find.donar.page')->middleware('auth');
Route::post('/find-donar', [HomeController::class, 'findDonarResult'])->name('find.donar.result')->middleware('auth');
Route::get('/change-status/{status}', [HomeController::class, 'changeStatus'])->name('change.status')->middleware('auth');
Route::get('/profile', [HomeController::class, 'profilePage'])->name('profile.page')->middleware('auth');
Route::post('/profile', [HomeController::class, 'updateProfile'])->name('update.profile')->middleware('auth');
Route::get('/profile/remove', [HomeController::class, 'removeProfilePic'])->name('remove.profile.pic')->middleware('auth');
Route::get('/profile/change-password', [HomeController::class, 'changePasswordPage'])->name('password.profile.page')->middleware('auth');
Route::post('/profile/change-password', [HomeController::class, 'changePassword'])->name('password.profile.change')->middleware('auth');

Route::get('/signin', [AuthController::class, 'loginPage'])->name('login.page');
Route::post('/signin', [AuthController::class, 'loginUser'])->name('login.user');
Route::get('/signup', [AuthController::class, 'registerPage'])->name('register.page');
Route::post('/signup', [AuthController::class, 'registerUser'])->name('register.user');
Route::get('/forgot-password', [AuthController::class, 'forgotPasswordPage'])->name('forgot.password.page');
Route::post('/forgot-password', [AuthController::class, 'forgotPassword'])->name('forgot.password');
Route::get('/signout', [AuthController::class, 'logout'])->name('logout');